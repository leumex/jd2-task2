package by.jd2.hib.jdbc;

import by.jd2.hib.jdbc.dao.DataSource;
import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Random;

public class FillingIn {
    public static void main(String[] args) throws SQLException {
        DataSource ds = DataSource.getInstance();
        Connection con = ds.getConnection();
        Random rd = new Random();

        /*Filling in Working_place table*/
        String[] position = {"JUNIOR", "MIDDLE", "SENIOR", "LEAD", "PROJECT_MANAGER"};
        for (int i = 0; i < 1500; i++) {
            PreparedStatement ps = con.prepareStatement("INSERT INTO working_place VALUES (?,?,?,?);");
            try {
                ps.setString(1, position[rd.nextInt(5)]);
                ps.setInt(2, (rd.nextInt(1497) + 1));
                ps.setInt(3, (rd.nextInt(22) + 1));
                ps.setInt(4, (rd.nextInt(400 + 1)));
                ps.executeUpdate();
            } catch (MySQLIntegrityConstraintViolationException e) {
                i--;
            }
            ;
            ps.close();
        }

        /*Filling in Office table*/
     /*   Statement st = con.createStatement();
        for (int i = 0; i < 400; i++) {
            st.addBatch("insert into office(`Address_id address`) VALUES ("+ (rd.nextInt(1001)+1) + ")");
        }
        st.executeBatch();
        st.close();
        con.close();
*/

        /*Filling in Employee table*/
 /*       String base = "qwerytuiopasdfghklzxcvbnm";
        Statement st = con.createStatement();
        for (int n = 0; n < 500; n++) {
            StringBuilder sb = new StringBuilder();
            int i = rd.nextInt(8) + 1;
            int j = rd.nextInt(13) + 1;
            int k = 0;
            char[] name = new char[i];
            char[] surname = new char[j];
            while (k < i) {
                int l = rd.nextInt(24);
                sb.append(base.charAt(l));
                k++;
            }
            String firstName = sb.toString();
            sb = new StringBuilder();
            k = 0;
            while (k < j) {
                int l = rd.nextInt(24);
                sb.append(base.charAt(l));
                k++;
            }
            String lastName = sb.toString();
            st.addBatch("INSERT INTO employee(first_name, last_name, home_address) VALUES('" +
                    firstName + "','" + lastName +"',"+(rd.nextInt(500)+1)+ ");");
        }
        st.executeBatch();
        st.close();
        con.close();*/


        /*Filling in Company table*/
       /* String sql = "INSERT INTO `company`(title) VALUES (?)";
        PreparedStatement ps = con.prepareStatement(sql);
        for (Company s : Company.values()) {
            ps.setString(1, s.toString());
            ps.execute();
        } ps.close();
        con.close();*/

        /*Filling in Address table*/
        /*Statement st = con.createStatement();
        for(int i=0; i<1001; i++){
            int id = rd.nextInt(230)+1;
                     st.addBatch("INSERT INTO address(details, `City_id city`) VALUES ('"+ UUID.randomUUID().toString().substring(rd.nextInt(29)+1) +"',"+id+" );");
        }
        st.executeBatch();
        st.close();
        con.close();*/


        /*Filling in Country table*/
        /*String sql = "INSERT INTO ` country`(title) VALUES (?)";
        for (Countries s : Countries.values()) {
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setString(1, s.toString());
            ps.execute();
        }*/

        /*Filling in cities*/
       /* String city = "*Australia,Sydney,Melbourne,Brisbane,Perth,Adelaide,Gold Coast,Newcastle,Canberra,*Austria,Vienna,Graz,Linz,Salzburg,Innsburk,*Belarus,Minsk,Gomel,Mogilev,Grodno,Vitebsk,Brest,Babruysk,*Belgium,Antwerp,Bruges,Brussels,Charleroi,Ghent,*Brazil,São Paulo,Rio de Janeiro,Brasília,Salvador,Fortaleza,Belo Horizonte,Manaus,Curitiba,Recife,Porto Alegre,*China,Beijing,Hong Kong,Macau,ShanghaiTianjin,*Croatia,Osijek,Rijeka,Split,Zagreb,*Czech_Republic,Brno,Liberec,Olomouc,Ostrava,Pilsen,Prague,*Denmark,Copenhagen,Århus,Odense,Aalborg,Frederiksberg,*Egypt,Banha,Bani Suwayf,Cairo,Damanhur,El-Mahalla El-Kubra,Giza,Imbaba,Ismaïlia,Kafr Dawar,Kena,Luxor,Port Said,*Finland,Helsinki,Espoo,Tampere,Vantaa,Oulu,*France,Paris,Marseille,Lyon,Toulouse,Nice,Nantes,Strasbourg,Montpellier,Bordeaux,Lille,*Germany,Aachen,Berlin,Bielefeld,Bremen,Bremerhaven,Cologne,Dortmund,Dresden,Duisburg,Frankfurt,Gelsenkirchen,Hanover,Mainz,Mannheim,Mönchengladbach,Munich,Mülheim,Münster,*Greece,Athens,Heraklion,Ioannina,Kallithea,Larissa,Patra,Piraeus,Peristeri,Thessaloniki,*India,Alwar,Ambala,Bally,Calcutta,Delhi,Guna,Mahbubnagar,*Indonesia,Ambon,Batu,Jakarta,Jayapura,Malang,Pematangsiantar,Surakarta,Yogyakarta,*Italy,Genoa,Giugliano in Campania,Milan,Modena,Piacenza,Prato,Ravenna,Rome,*Japan,Nagasaki,Okinawa,Osaka,Tokyo,*Latvia,Riga,*Malaysia,Alor Setar,Kuala Lumpur,Malacca City,Petaling Jaya,Sungai Petani,Taiping,*Mexico,Mexico City,Nogales,Tijuana,Torreón,Tulancingo,*Netherlands,Amersfoort,Amsterdam,Apeldoorn,Arnhem,Breda,Dordrecht,Eindhoven,Maastricht,Nijmegen,Rotterdam,*Norway,Oslo,Bergen,Trondheim,Stavanger,Bærum,*Poland,Białystok,Kraków,Warsaw,Wrocław,Zielona Góra,*Portugal,Braga,Coimbra,Lisbon,Porto,*Russia,Arkhangelsk,Bryansk,Elektrostal,Kaluga,Kursk,Moscow,Novosibirsk,Saint Petersburg,*Slovakia,Bratislava,Košice,*South_Korea,Andong,Daejeon,Hanam,Pohang,Seoul,Yeosu,*Spain,Alicante,Barcelona,Bilbao,Granada,Madrid,Valencia,Zaragoza,*Sweden,Stockholm,Gothenburg,Malmö,Uppsala,Örebro,Norrköping,*Ukraine,Berdiansk,Donetsk,Kharkiv,Kherson,Khmelnytskyi,Kiev,Lviv,*Great_Britain,Doncaster,Dudley,Dundee,Edinburgh,Liverpool,London,Luton,Maidstone,Manchester,Middlesbrough,*United_States,New York,Los Angeles,Chicago,Houston,Phoenix,Philadelphia,San Antonio,San Diego,Dallas,San Jose,*Vietnam,Buôn Ma Thuột,Cẩm Phả,Hà Nội,Hồ Chí Minh City,";
        char a = "*".charAt(0);
        char b = ",".charAt(0);
        int id=0;
        char[] cityArray = city.toCharArray();
        Statement st = con.createStatement();
        for (int i = 0; i <= cityArray.length - 1; i++) {
            if (cityArray[i] == a) {
                String state = retrieveTitle(i, cityArray, b);
                Statement stt = con.createStatement();
                ResultSet rs = stt.executeQuery("SELECT `id country` FROM ` country`WHERE title ='"+state+"';");
                while(rs.next()){id = rs.getInt(1);}
                stt.close();
            }
            if (cityArray[i] == b & i != cityArray.length - 1) {
                if (cityArray[i + 1] != a) {
                    String location = retrieveTitle(i, cityArray, b);
                    st.addBatch("INSERT INTO city(title, ` Country_id country`) VALUES ('"+ location +"',"+id+");");
                }
            }
        }
        int[] updateCounts = st.executeBatch();
        st.close();
        con.close();*/
    }

    private static String retrieveTitle(int i, char[] sequence, char b) {
        StringBuilder sb = new StringBuilder();
        i++;
        while (sequence[i] != b) {
            sb.append(sequence[i]);
            i++;
        }
        return sb.toString();
    }
}
