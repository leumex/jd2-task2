package by.jd2.hib.jdbc.dao;

import by.jd2.hib.jdbc.bean.Employee;
import by.jd2.hib.jdbc.bean.Office;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DAOImpl implements DAO {

    @Override
    public Map<Employee, List<Office>> read(int quantity) {
        Map<Employee, List<Office>> resultMap;
        try {
            Connection con = DataSource.getInstance().getConnection();
            String sql = "select concat_ws(' ',employee.first_name, employee.last_name) as name,\n" +
                    "       concat_ws(', ',address.details, c.title, c2.title)as home_address,\n" +
                    "       company.title as company, city.title as city, ` country`.title as country,\n" +
                    "       a.details as office_address,\n" +
                    "       ( select count(`Employee_id employee` )from working_place\n" +
                    "       where working_place.Office_Id = wp.Office_Id)\n" +
                    "           as office_staff,\n" +
                    "       wp.position as position\n" +
                    "from working_place wp\n" +
                    "join employee on wp.`Employee_id employee` = employee.`id employee`\n" +
                    "join address on employee.home_address = address.`id address`\n" +
                    "join office on office.Id = wp.Office_Id\n" +
                    "join address a on a.`id address`= office.`Address_id address`\n" +
                    "join company on wp.`Company_id company` = company.`id company`\n" +
                    "join city on city.`id city`= a.`City_id city`\n" +
                    "join city c on c.`id city` = address.`City_id city`\n" +
                    "join ` country` c2 on c.` Country_id country` = c2.`id country`\n" +
                    "join ` country` on ` country`.`id country`= city.` Country_id country`\n" +
                    "order by name;";
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(sql);
            resultMap = parseResult(rs, quantity);
            con.close();
            st.close();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            return null;
        }
        return resultMap;
    }

    private Map<Employee, List<Office>> parseResult(ResultSet rs, int quantity) throws SQLException {
        Map<Employee, List<Office>> parseResult = new HashMap<>();
        while (rs.next()) {
            if(parseResult.size()>=quantity ) return parseResult;
                String name = rs.getString("name");
                String home_address = rs.getString("home_address");
                Employee employee = new Employee(name, home_address);
            String company = rs.getString("company");
            String city = rs.getString("city");
            String country = rs.getString("country");
            String office_address = rs.getString("office_address");
            int office_staff = rs.getInt("office_staff");
            String position = rs.getString("position");
            Office office = new Office.Builder().
                    setAddress(office_address).
                    setCity(city).
                    setCountry(country).
                    setCompany(company).
                    setStaff(office_staff).
                    setPosition(position).
                    build();
            if (parseResult.containsKey(employee)){
                List<Office>currentList = parseResult.get(employee);
                currentList.add(office);
                parseResult.put(employee, currentList);
            }else {
                List<Office> offices = new ArrayList<>();
                offices.add(office);
                parseResult.put(employee, offices);
            }
        }
        return parseResult;
    }
}
