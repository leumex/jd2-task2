package by.jd2.hib.jdbc.dao;

import by.jd2.hib.jdbc.bean.Employee;
import by.jd2.hib.jdbc.bean.Office;
import java.util.List;
import java.util.Map;

public interface DAO {
    Map<Employee, List<Office>> read(int quantity);
}
