package by.jd2.hib.jdbc.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DataSource {
    private static final DataSource INSTANCE = new DataSource();

    static {
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            System.out.println(e.getMessage() + " Driver is NOT registered!");
        }
    }
    public static DataSource getInstance() {
        return INSTANCE;
    }

    private DataSource() {
    }

    public Connection getConnection() throws SQLException {
        return DriverManager.getConnection("jdbc:mysql://localhost:3306/task2database", "root", "1234");
    }
}