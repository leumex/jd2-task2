package by.jd2.hib.jdbc.bean;

import java.util.Objects;

public class Employee {
    private String name;
    private String home_address;

    public Employee(String name, String home_address) {
        this.name = name;
        this.home_address = home_address;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getHome_address() {
        return home_address;
    }

    public void setHome_address(String home_address) {
        this.home_address = home_address;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Employee employee = (Employee) o;
        return getName().equals(employee.getName()) &&
                getHome_address().equals(employee.getHome_address());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getName(), getHome_address());
    }

    @Override
    public String toString() {
        return "\n" + name + ", lives at " + home_address;
    }
}
