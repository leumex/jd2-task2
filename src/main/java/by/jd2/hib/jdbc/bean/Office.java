package by.jd2.hib.jdbc.bean;

public class Office {
    private String company;
    private String office_city;
    private String office_country;
    private String office_address;
    private int office_staff;
    private String position;


    public static class Builder {
        private Office office = new Office();

        public Builder setCompany(String company) {
            office.company = company;
            return this;
        }
        public Builder setCity (String city){
            office.office_city = city;
            return this;
        }
        public Builder setCountry (String country){
            office.office_country = country;
            return this;
        }
        public Builder setAddress (String address){
            office.office_address = address;
            return this;
        }
        public Builder setStaff (int staff){
            office.office_staff = staff;
            return this;
        }
        public Builder setPosition (String position){
            office.position = position;
            return this;
        }
        public Office build(){
            return office;
        }


    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getOffice_city() {
        return office_city;
    }

    public void setOffice_city(String office_city) {
        this.office_city = office_city;
    }

    public String getOffice_country() {
        return office_country;
    }

    public void setOffice_country(String office_country) {
        this.office_country = office_country;
    }

    public String getOffice_address() {
        return office_address;
    }

    public void setOffice_address(String office_address) {
        this.office_address = office_address;
    }

    public int getOffice_staff() {
        return office_staff;
    }

    public void setOffice_staff(int office_staff) {
        this.office_staff = office_staff;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    @Override
    public String toString() {
        return "\noffice: " + company +", " + office_city + ", " + office_country + ", " + office_address +
                ", office_staff = " + office_staff +
                ", position is " + position;
    }
}
