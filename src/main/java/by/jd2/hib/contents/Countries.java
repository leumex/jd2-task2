package by.jd2.hib.contents;

public enum Countries {
    Germany, Austria, Belarus, Russia, Greece, Poland, France, China, Australia, Italy, Sweden, Czech_Republic, Norway,
    Great_Britain, Spain, Finland, Latvia, Denmark, Belgium, Netherlands, Slovakia,
    Portugal, Ukraine, Japan, Brazil, United_States, Mexico, Croatia, Egypt, India, South_Korea, Vietnam, Indonesia, Malaysia}
